void main(List<String> args) {
  var winningAppsFor2012 = [
    'FNB Banking App',
    'Discovery Health ID App',
    'TransUnion Dealer Guide',
    'Rapidtargets',
    'Matchy',
    'Plascon Insprire Me',
    'PhraZApp'
  ];

  winningAppsFor2012.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2013 = [
    'DSTV',
    '.comm Telco Data Visualize',
    'PriceCheck Mobile',
    'MarkitShare',
    'Netbank App Suite',
    'SnapScan',
    'Kids Aid'
  ];

  winningAppsFor2013.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2014 = [
    'Live Inspect',
    'Vigo',
    'Zapper',
    'Rea Vaya',
    'Wildlife Tracker',
    'SuperSport',
    'SyncMobile',
    'My Belongings'
  ];

  winningAppsFor2014.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2015 = [
    'WumDrop',
    'DStv Now',
    'Vula Mobile',
    'CPUT Mobile',
    'EskomSePush',
    'M4JAM'
  ];

  winningAppsFor2015.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2016 = [
    'Domestly',
    'iKhokha',
    'HearZA',
    'Tuta-me',
    'KaChing',
    'Friendly Math Monster for Kindergarten'
  ];

  winningAppsFor2016.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2017 = [
    'TransUnion 1Check',
    'OrderIN',
    'EcoSlips',
    'InterGreetMe',
    'Zulzi',
    'Hey Jude',
    'Pick n Pays Super Animals 2',
    'The Tree App South Africa',
    'WatIf Health Portal',
    'Awethu Project',
    'Shyft for Standard Bank'
  ];

  winningAppsFor2017.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2018 = [
    'Khula Ecosystem',
    'Pineapple',
    'Cowa Bunga',
    'Digemy Knowledge Partner and Besmarter',
    'Bestee',
    'The African Gaming League App (ACGL)',
    'dbTrack',
    'Stokfella',
    'Difela Hymns',
    'Xander English 1-20',
    'Ctrl',
    'ASI Snakes'
  ];

  winningAppsFor2018.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2019 = [
    'Digger',
    'SI Realities',
    'Vula Mobile',
    'Hydra Farm',
    'Matric Live',
    'Franc',
    'Over',
    'LocTransi',
    'Naked Insurance',
    'Loot Defense',
    'MoWash'
  ];

  winningAppsFor2019.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2020 = [
    'Easy Equities',
    'Examsta',
    'Checkers Sixty60',
    'Techishen',
    'BirdPro',
    'Lexi Hearing',
    'League of Legends',
    'GreenFingers Mobile',
    'Xitsonga Dictionary',
    'StokFella',
    'Bottles',
    'Matric Live',
    'Guardian Health',
    'My Pregnant Journey'
  ];

  winningAppsFor2020.sort((String l, String r) {
    return l.compareTo(r);
  });

  var winningAppsFor2021 = [
    'iiDENTIFii app',
    'Hellopay SoftPOS',
    'Guardian Health Platform',
    'Ambani Africa',
    'Murimi',
    'Shyft',
    'Sisa',
    'UniWise',
    'Kazi',
    'Takealot app',
    'Rekindle Learning app',
    'Roadsave',
    'Afrihost'
  ];

  winningAppsFor2021.sort((String l, String r) {
    return l.compareTo(r);
  });

  print("The winning apps for 2017 are $winningAppsFor2017");

  print("\n");

  print("The winning apps for 2013 are $winningAppsFor2013");

  print("\n");

  print("The total number of winning apps is : ");
  print(winningAppsFor2012.length +
      winningAppsFor2013.length +
      winningAppsFor2014.length +
      winningAppsFor2015.length +
      winningAppsFor2016.length +
      winningAppsFor2017.length +
      winningAppsFor2018.length +
      winningAppsFor2019.length +
      winningAppsFor2020.length +
      winningAppsFor2021.length);
}
