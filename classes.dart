void main(List<String> args) {
  WinningAppInfo infoSet = WinningAppInfo();
  infoSet.informationEntered(
      'Naked Insurance',
      'Best Financial Solution Award and the Overall winner',
      'Sumarie Greybe, Ernest North and Alex Thomson',
      2019);
  infoSet.informationDisplay();

  print('\n');

  infoSet.nameOfAppCapitalized();
}

class WinningAppInfo {
  // define the variables
  String? nameOfTheApp;
  String? sectorOrCatergory;
  String? developer;
  int? winningYear;

  // create a method to allow for the variables to be given values
  void informationEntered(String nameOfTheApp1, String sectorOrCatergory1,
      String developer1, int winningYear1) {
    //print out the above variables
    nameOfTheApp = nameOfTheApp1;
    sectorOrCatergory = sectorOrCatergory1;
    developer = developer1;
    winningYear = winningYear1;
  }

  // create a method to display the information
  void informationDisplay() {
    //print out the above variables
    print('The name of the app is: $nameOfTheApp');
    print('The sector/catergory: $sectorOrCatergory');
    print('The developer is: $developer');
    print(
        'The year the app won the MTN Business App of the Year Awards is: $winningYear');
  }

  void nameOfAppCapitalized() {
    nameOfTheApp = nameOfTheApp?.toUpperCase();
    print('Name of the winning app in capital letters: $nameOfTheApp');
  }
}
